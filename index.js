

function fetchData(){
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(json => {
   let aray = json.map((elem) =>{
      return (elem.title)
   });console.log(aray)
});

};
fetchData();

// code solution for postman

// Get todoList

fetch('https://jsonplaceholder.typicode.com/todos',{
   method: 'GET',
  

})
.then((response) => response.json())
.then((json)=> console.table(json));

//Get specific todoList

fetch('https://jsonplaceholder.typicode.com/todos/1',{
   method: 'GET',

})
.then((response) => response.json())
.then((json)=> console.table(json));

// Post todoList

fetch('https://jsonplaceholder.typicode.com/todos',{
   method: 'POST',
   headers: {
      "Content-Type" : "application/json",
   },
   body: JSON.stringify({
      "userId" : 1,
      "id" : 201,
      "title" : "Zuitt Bootcamp",
      "completed" : true
   })
})
.then((response) => response.json())
.then((json)=> console.table(json));


// Put todoList

fetch('https://jsonplaceholder.typicode.com/todos/1',{
   method: 'PUT',
   headers: {
      "Content-Type" : "application/json",
   },
   body: JSON.stringify({
      "userId" : 5,
      "id" : 201,
      "title" : "Zuitt Bootcamp",
      "completed" : true
   })
})
.then((response) => response.json())
.then((json)=> console.table(json));

// Patch todoList


fetch('https://jsonplaceholder.typicode.com/todos/1',{
   method: 'PATCH',
   headers: {
      "Content-Type" : "application/json",
   },
   body: JSON.stringify({
      "userId" : 5,
      "id" : 201,
      "title" : "Zuitt Bootcamp",
      "completed" : false
   })
})
.then((response) => response.json())
.then((json)=> console.table(json));

// Delete specific todoList


fetch('https://jsonplaceholder.typicode.com/todos/1', {
   method: 'DELETE'
})

.then((response) => response.json())
.then((json)=> console.table(json));